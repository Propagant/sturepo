﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

namespace Yahart
{
    public class ssMainMenu : MonoBehaviour
    {
        private Camera mainCam;
        public ARSession ARorigin;
        public GameObject[] ARComps;
        public arAdd_ObjectObserver ARObserver;
        public ssProgramDatabase sProgramDatabase;
        [Space]
        public Animation uiBlackFade;

        [System.Serializable]
        public class sUI
        {
            [Header("System Essentials")]
            public GameObject Ess_CanvasBackground;
            public GameObject Ess_CanvasWhiteBG;
            [Space]
            public Animation Ess_MenuPanel;
            public Animation Ess_AvailableCatsPanel;
            [Space]
            public Animation Ess_Settings;
            public Animation Ess_About;

            [Header("Editor")]
            public GameObject Editor_UI;
            public GameObject Editor_ARUI;
            public GameObject EditorObjs;

            [Header("Object Viewer")]
            public Transform ObjView_EssentialDropDown;
            public Button ObjView_DropDownPrefabBut;
            public Text ObjView_DropDownLabel;
            [Space]
            public Animation ObjView_Rp_ObjParts;
            public Animation ObjView_Rp_Angles;
            public Animation ObjView_Rp_Info;
            public Animation ObjView_Rp_Anim;
            public Animation ObjView_Rp_Model;
            [Space]
            public Sprite ObjViewIMG_Selection;
            public Sprite ObjViewIMG_Default;
        }
        public sUI sUi;
        [Space]
        public bool sInAR;

        private void Awake()
        {
            Application.targetFrameRate = 60;
            foreach (GameObject gm in ARComps)
            {
                foreach (MonoBehaviour mono in gm.GetComponents<MonoBehaviour>())
                    mono.enabled = false;
            }
            mainCam = Camera.main;
        }

        private void Start()
        {
            ssVIEW_dropdownValueSelected("0");

            foreach (ssProgramDatabase.TemporaryClass temp in sProgramDatabase.temporaryClass)
                temp.sSource.gameObject.SetActive(false);

            for (int i = sUi.ObjView_EssentialDropDown.childCount - 1; i >= 0; i--)
                Destroy(sUi.ObjView_EssentialDropDown.GetChild(i).gameObject);

            sView_SelectedFeature = 0;
            for (int i = 0; i < sProgramDatabase.temporaryClass.Length; i++)
            {
                Button b = Instantiate(sUi.ObjView_DropDownPrefabBut, sUi.ObjView_EssentialDropDown);
                b.transform.localPosition = Vector3.zero;
                b.transform.localRotation = Quaternion.identity;
                b.transform.localScale = Vector3.one;
                b.name = i.ToString();
                b.GetComponentInChildren<Text>().text = sProgramDatabase.temporaryClass[i].sName.Remove(0, 4);
                b.onClick.AddListener(delegate
                {
                    ssVIEW_dropdownValueSelected(b.name);
	            });
            }
        }

        #region Program Essentials

        public void ssMENU_SelectDownloadedCategory(int i)
        {
            StartCoroutine(ssSelectCatDelay());
        }
        private IEnumerator ssSelectCatDelay()
        {
            uiBlackFade.Play();

            yield return new WaitForSeconds(1);

            ssVIEW_dropdownValueSelected("0");
            sUi.Editor_UI.SetActive(true);
            sUi.Ess_MenuPanel.gameObject.SetActive(false);
            sUi.Ess_Settings.gameObject.SetActive(false);
            sUi.Ess_About.gameObject.SetActive(false);
            sUi.Ess_AvailableCatsPanel.gameObject.SetActive(false);
            sUi.EditorObjs.SetActive(true);

            yield return new WaitForSeconds(0.1f);

            sUi.ObjView_DropDownLabel.text = "Výber model noža";
        }

        public void ssMENU_GoHome()
        {
            StartCoroutine(ssGoHome());
        }
        private IEnumerator ssGoHome()
        {
            uiBlackFade.Play();
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(0);
        }

        public void ssMENU_StartAR(bool inoutAR)
        {
            sInAR = inoutAR;
            StartCoroutine(ssAR(inoutAR));
        }
        private IEnumerator ssAR(bool arIn)
        {
            uiBlackFade.Play();

            yield return new WaitForSeconds(1);

            if (arIn) ARObserver.ResetToARTransform();
            else ARObserver.ResetToOriginalTransform();
            sUi.Editor_UI.SetActive(!arIn);
            sUi.Editor_ARUI.SetActive(arIn);
            sUi.Ess_CanvasBackground.SetActive(!arIn);
            sUi.Ess_CanvasWhiteBG.SetActive(!arIn);
            ARorigin.Reset();
            foreach (GameObject gm in ARComps)
            {
                foreach (MonoBehaviour mono in gm.GetComponents<MonoBehaviour>())
                    mono.enabled = arIn;
            }
        }

        public void ssMENU_ShowPanel(int i)
        {
            switch (i)
            {
                //Back
                case 0:
                    if (sUi.Ess_MenuPanel.clip.name.ToLower().Contains("hide"))
                    {
                        sUi.Ess_MenuPanel.clip = sUi.Ess_MenuPanel.GetClip("uiMenu_ShowMainMenu");
                        sUi.Ess_MenuPanel.Play();
                    }
                    if (sUi.Ess_AvailableCatsPanel.clip.name.ToLower().Contains("open"))
                    {
                        sUi.Ess_AvailableCatsPanel.clip = sUi.Ess_AvailableCatsPanel.GetClip("uiMenu_CloseAvailableCats");
                        sUi.Ess_AvailableCatsPanel.Play();
                    }

                    if (sUi.Ess_Settings.clip.name.ToLower().Contains("show"))
                    {
                        sUi.Ess_Settings.clip = sUi.Ess_Settings.GetClip("uiMenu_HideSettings");
                        sUi.Ess_Settings.Play();
                    }
                    if (sUi.Ess_About.clip.name.ToLower().Contains("show"))
                    {
                        sUi.Ess_About.clip = sUi.Ess_About.GetClip("uiMenu_HideAbout");
                        sUi.Ess_About.Play();
                    }
                    break;

                //Available categories
                case 1:
                    sUi.Ess_AvailableCatsPanel.clip = sUi.Ess_AvailableCatsPanel.GetClip("uiMenu_OpenAvailableCats");
                    sUi.Ess_AvailableCatsPanel.Play();
                    break;

                //Settings
                case 2:
                    sUi.Ess_MenuPanel.clip = sUi.Ess_MenuPanel.GetClip("uiMenu_HideMainMenu");
                    sUi.Ess_MenuPanel.Play();
                    sUi.Ess_Settings.clip = sUi.Ess_Settings.GetClip("uiMenu_ShowSettings");
                    sUi.Ess_Settings.Play();
                    break;
                //About
                case 3:
                    sUi.Ess_MenuPanel.clip = sUi.Ess_MenuPanel.GetClip("uiMenu_HideMainMenu");
                    sUi.Ess_MenuPanel.Play();
                    sUi.Ess_About.clip = sUi.Ess_About.GetClip("uiMenu_ShowAbout");
                    sUi.Ess_About.Play();
                    break;
            }
        }

        public void ssMENU_OpenUrl(string u)
        {
            Application.OpenURL(u);
        }

        #endregion


        #region Object Viewer

        //------------Essential methods
        private int sView_SelectedModel;
        private int sView_SelectedFeatureIndex = 0;
        private void ssVIEW_dropdownValueSelected(string senderIndex)
        {
            sView_SelectedFeatureIndex = int.Parse(senderIndex);

            switch (sView_SelectedFeature)
            {
                //-----Models
                case 0:
                    {
                        ssVIEW_resetModeltoDefault(sView_SelectedModel);
                        foreach (ssProgramDatabase.TemporaryClass temp in sProgramDatabase.temporaryClass)
                            temp.sSource.gameObject.SetActive(false);
                        sView_SelectedModel = sView_SelectedFeatureIndex;
                        sProgramDatabase.temporaryClass[sView_SelectedModel].sSource.SetActive(true);

                        List<(GameObject target, string active)> queueList = new List<(GameObject target, string active)>();

                        string tAnim = sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles.Count == 0
                            ? "uiViewer_Show_IconCat" : "uiViewer_Hide_IconCat";
                        if (sUi.ObjView_Rp_Angles.clip.name != tAnim)
                        {
                            sUi.ObjView_Rp_Angles.clip = sUi.ObjView_Rp_Angles.GetClip(tAnim);
                            queueList.Add((sUi.ObjView_Rp_Angles.gameObject, sUi.ObjView_Rp_Angles.clip.name));
                            sUi.ObjView_Rp_Angles.Play();
                        }
                        tAnim = sProgramDatabase.temporaryClass[sView_SelectedModel].sAnimations.Length == 0
                            ? "uiViewer_Show_IconCat" : "uiViewer_Hide_IconCat";
                        if (sUi.ObjView_Rp_Anim.clip.name != tAnim)
                        {
                            sUi.ObjView_Rp_Anim.clip = sUi.ObjView_Rp_Anim.GetClip(tAnim);
                            queueList.Add((sUi.ObjView_Rp_Anim.gameObject, sUi.ObjView_Rp_Anim.clip.name));
                            sUi.ObjView_Rp_Anim.Play();
                        }
                        tAnim = sProgramDatabase.temporaryClass[sView_SelectedModel].sInformation.Length == 0
                            ? "uiViewer_Show_IconCat" : "uiViewer_Hide_IconCat";
                        if (sUi.ObjView_Rp_Info.clip.name != tAnim)
                        {
                            sUi.ObjView_Rp_Info.clip = sUi.ObjView_Rp_Info.GetClip(tAnim);
                            queueList.Add((sUi.ObjView_Rp_Info.gameObject, sUi.ObjView_Rp_Info.clip.name));
                            sUi.ObjView_Rp_Info.Play();
                        }
                        tAnim = sProgramDatabase.temporaryClass[sView_SelectedModel].sParts.Count == 0
                            ? "uiViewer_Show_IconCat" : "uiViewer_Hide_IconCat";
                        if (sUi.ObjView_Rp_ObjParts.clip.name != tAnim)
                        {
                            sUi.ObjView_Rp_ObjParts.clip = sUi.ObjView_Rp_ObjParts.GetClip(tAnim);
                            queueList.Add((sUi.ObjView_Rp_ObjParts.gameObject, sUi.ObjView_Rp_ObjParts.clip.name));
                            sUi.ObjView_Rp_ObjParts.Play();
                        }
                        StartCoroutine(ssVIEW_ActivationQueue(queueList));
                    };
                    break;

                //-----Animations
                case 2:
                    if (!sProgramDatabase.temporaryClass[sView_SelectedModel].sSource.GetComponent<ssProceduralAnimator>()) return;

                    ssProceduralAnimator anim = sProgramDatabase.temporaryClass[sView_SelectedModel].sSource.GetComponent<ssProceduralAnimator>();
                    if (sView_SelectedFeatureIndex == -1)
                        anim.ssRESET_ELEMENTS_TO_DEFAULT(true);
                    else
                    {
                        anim.ssLOAD_SOURCE(sProgramDatabase.temporaryClass[sView_SelectedModel].sAnimations[sView_SelectedFeatureIndex], true);
                        anim.ssPROCESS_ANIMATION();
                    }
                    break;

                //-----Angles
                case 3:
                    foreach (GameObject a in sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles)
                        a.gameObject.SetActive(false);
                    if (sView_SelectedFeatureIndex == -1)
                    {}
                    else if (sView_SelectedFeatureIndex == -2)
                    {
                        foreach (GameObject a in sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles)
                            a.gameObject.SetActive(true);
                    }
                    else
                        sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles[sView_SelectedFeatureIndex].SetActive(true);
                    break;

                //-----Parts
                case 4:
                    if (processingPartShow) return;

                    foreach (Material a in sProgramDatabase.temporaryClass[sView_SelectedModel].sParts)
                        a.SetFloat("_Alpha", 0.0f);

                    if(sView_SelectedFeatureIndex >= 0)
                        StartCoroutine(ssVIEW_ShowPart(sProgramDatabase.temporaryClass[sView_SelectedModel].sParts[sView_SelectedFeatureIndex]));

                    break;
            }

            sUi.ObjView_EssentialDropDown.parent.parent.gameObject.SetActive(false);
        }

        private IEnumerator ssVIEW_ActivationQueue(List<(GameObject sender, string active)> sender)
        {
            List<GameObject> separedA = new List<GameObject>();
            List<GameObject> separedB = new List<GameObject>();

            foreach ((GameObject gm, string a) ss in sender)
            {
                if (ss.a.ToLower().Contains("show")) separedA.Add(ss.gm);
                else separedB.Add(ss.gm);
            }
            foreach (GameObject sep in separedB)
                sep.SetActive(true);
            yield return new WaitForSeconds(0.4f);
            foreach (GameObject sep in separedA)
                sep.SetActive(false);
        }

        private void ssVIEW_resetModeltoDefault(int modelIndex)
        {
            foreach (GameObject a in sProgramDatabase.temporaryClass[modelIndex].sAngles)
                a.gameObject.SetActive(false);
            foreach (Material a in sProgramDatabase.temporaryClass[modelIndex].sParts)
                a.SetFloat("_Alpha", 0.0f);
            if (!sProgramDatabase.temporaryClass[modelIndex].sSource.GetComponent<ssProceduralAnimator>()) return;
            sProgramDatabase.temporaryClass[modelIndex].sSource.GetComponent<ssProceduralAnimator>().ssRESET_ELEMENTS_TO_DEFAULT(true);
        }

        private bool processingPartShow = false;
        private IEnumerator ssVIEW_ShowPart(Material p)
        {
            processingPartShow = true;
            float t = 0.0f;
            float s = 0.4f;
            while(t < s)
            {
                p.SetFloat("_Alpha", Mathf.Lerp(0.0f, 0.5f, t / s));
                t += Time.deltaTime;
                yield return null;
            }
            processingPartShow = false;
        }

        private int sView_SelectedFeature = 0;
        public void ssVIEW_CallForBranchFeatures(RectTransform sender)
        {
            for (int i = sUi.ObjView_EssentialDropDown.childCount - 1; i >= 0; i--)
                Destroy(sUi.ObjView_EssentialDropDown.GetChild(i).gameObject);

            sUi.ObjView_Rp_Angles.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Default;
            sUi.ObjView_Rp_Anim.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Default;
            sUi.ObjView_Rp_Info.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Default;
            sUi.ObjView_Rp_Model.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Default;
            sUi.ObjView_Rp_ObjParts.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Default;
            sUi.ObjView_EssentialDropDown.parent.parent.gameObject.SetActive(false);

            int selected = 0;
            switch (sender.name)
            {
                case "bMODEL":
                    sUi.ObjView_Rp_Model.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Selection;

                    selected = 0;
                    for (int i = 0; i < sProgramDatabase.temporaryClass.Length; i++)
                        ssVIEW_CreateDropdownBut(i, sProgramDatabase.temporaryClass[i].sName.Remove(0, 4));
                    sUi.ObjView_DropDownLabel.text = "Výber model noža";
                    break;

                case "bINFO":
                    sUi.ObjView_Rp_Info.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Selection;

                    selected = 1;
                    for (int i = 0; i < sProgramDatabase.temporaryClass[sView_SelectedModel].sInformation.Length; i++)
                        ssVIEW_CreateDropdownBut(i, sProgramDatabase.temporaryClass[sView_SelectedModel].sInformation[i].Substring(0, 5) + "...");
                    sUi.ObjView_DropDownLabel.text = "Výber informácií noža";
                    break;

                case "bPLAY":
                    sUi.ObjView_Rp_Anim.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Selection;

                    selected = 2;
                    for (int i = 0; i < sProgramDatabase.temporaryClass[sView_SelectedModel].sAnimations.Length; i++)
                        ssVIEW_CreateDropdownBut(i, sProgramDatabase.temporaryClass[sView_SelectedModel].sAnimations[i].name);

                    ssVIEW_CreateDropdownBut(-1, "Vypnúť animácie");
                    sUi.ObjView_DropDownLabel.text = "Výber animácie noža";
                    break;

                case "bANGLE":
                    sUi.ObjView_Rp_Angles.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Selection;

                    selected = 3;
                    for (int i = 0; i < sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles.Count; i++)
                        ssVIEW_CreateDropdownBut(i, sProgramDatabase.temporaryClass[sView_SelectedModel].sAngles[i].name);

                    ssVIEW_CreateDropdownBut(-1, "Schovať roviny");
                    ssVIEW_CreateDropdownBut(-2, "Kompletné roviny");
                    sUi.ObjView_DropDownLabel.text = "Výber roviny noža";
                    break;

                case "bPARTS":
                    sUi.ObjView_Rp_ObjParts.transform.GetChild(0).Find("Bg").GetComponent<Image>().sprite = sUi.ObjViewIMG_Selection;

                    selected = 4;
                    for (int i = 0; i < sProgramDatabase.temporaryClass[sView_SelectedModel].sParts.Count; i++)
                        ssVIEW_CreateDropdownBut(i, sProgramDatabase.temporaryClass[sView_SelectedModel].sParts[i].name.Replace(" (Instance)", ""));

                    ssVIEW_CreateDropdownBut(-1, "Schovať plochy");
                    sUi.ObjView_DropDownLabel.text = "Výber plochy noža";
                    break;
            }
            sView_SelectedFeature = selected;
        }

        private void ssVIEW_CreateDropdownBut(int featureIndex, string txt)
        {
            Button b = Instantiate(sUi.ObjView_DropDownPrefabBut, sUi.ObjView_EssentialDropDown);
            b.transform.localPosition = Vector3.zero;
            b.transform.localRotation = Quaternion.identity;
            b.transform.localScale = Vector3.one;
            b.name = featureIndex.ToString();
            b.GetComponentInChildren<Text>().text = txt;
            b.onClick.AddListener(delegate
            {
                ssVIEW_dropdownValueSelected(b.name);
            });
        }

        private bool processingFocus = false;
        public void ssVIEW_FocusObject()
        {
            if (processingFocus) return;
            StartCoroutine(ssVIEW_Focusing());
        }
        private IEnumerator ssVIEW_Focusing()
        {
            processingFocus = true;
            float t = 0.0f;
            float s = 1;
            Vector3 spos = ARObserver.transform.localPosition;
            Quaternion srot = ARObserver.transform.localRotation;
            Vector3 ssca = ARObserver.transform.localScale;
            float curZ = ARObserver.observ_CurrentZoom;
            Vector3 curof = ARObserver.observ_Offset;
            float xof = ARObserver.xOf, yof = ARObserver.yOf;
            ARObserver.observ_CurrentSize = 0.4f;
            Vector3 campos = mainCam.transform.position;
            Quaternion camrot = mainCam.transform.rotation;
            while(t < s)
            {
                ARObserver.transform.localPosition = Vector3.Lerp(spos, Vector3.zero + ARObserver.observ_Offset, t / s);
                ARObserver.transform.localRotation = Quaternion.Lerp(srot, ARObserver.startRot, t / s);
                ARObserver.transform.localScale = Vector3.Lerp(ssca, Vector3.one * 0.4f, t / s);
                ARObserver.observ_CurrentZoom = Mathf.Lerp(curZ, 70, t / s);
                ARObserver.observ_Offset = Vector3.Lerp(curof, ARObserver.startOff, t / s);
                ARObserver.xOf = Mathf.Lerp(xof, 0, t / s);
                ARObserver.yOf = Mathf.Lerp(yof, 0, t / s);
                mainCam.transform.position = Vector3.Slerp(campos, Vector3.zero + Vector3.back * 1.8f, t / s);
                mainCam.transform.rotation = Quaternion.Slerp(camrot, Quaternion.identity, t / s);
                t += Time.deltaTime;
                yield return null;
            }
            processingFocus = false;
        }

        #endregion
    }
}