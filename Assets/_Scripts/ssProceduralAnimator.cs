﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yahart
{
    //---Procedural Animator written by Matej Vanco, November 2020 ©
    //
    //
    //              Syntax
    //-------------------------------
    //The very first line contains additional information, just for developer & user

    //The second line contains list of objects, that will be declared as animation elements
    //Example: Main=Body;
    //First is 'variable name' second is 'variable value', respectively name of the child in object
    //The object array must be divided with ; symbol
    //Basically the system will create an array of variables
    //!!!Objects should be located just under the main parent! No other sub-children!!!

    //The third line handles the animation settings
    //Example: Loop; 1.0; 0,0,0
    //First element means if the animation will loop or play once [Loop/Default]
    //Second element corresponds to the animation lifetime [1.0 (SECONDS) is default]
    //The third element is an additional starting offset for each animation entity [optional]

    //The fourth line handles objects activation (which object will be activated while not animating & viceversa)
    //Example: Main;Eye
    //Basically the list of objects that will be activated/deactivated if animation is in process

    //Other lines correspond to each objects animation

    //Commands are as follows...
    //
    //  Move direction = M0,0,0
    //      Command creates a motion to specific direction in LOCAL space
    //      Example: Head = M0,1,0
    //
    //  Rotate direction = R0,0,0
    //      Command creates a rotation to specific direction in LOCAL space
    //      Example: Head = R0,1,0
    //
    //  Rescale = S0,0,0
    //      Command creates a scale motion in LOCAL space
    //      Example: Head = S0.4,0.4,0.4
    //
    //
    //Other syntactic rules for the animator compiler:
    //  - Spaces are are irrelevant, compiler handles it all
    //  - New lines are irrelevant after required first two lines
    //  - Variable values must be equal to generic object names



    public class ssProceduralAnimator : MonoBehaviour
    {
        public struct sElementBuffer
        {
            public Transform src;
            public bool inAnimOnly;
            public Vector3 dPos, dRot, dSca;
            public Vector3 oPos, oRot, oSca;
            public string varName;
            public string varValue;
        }
        private List<sElementBuffer> sAnimElementsBuffer = new List<sElementBuffer>();

        private bool sLoopAnim = true;
        private float sLifeTime = 1.0f;
        private Vector3 sOffset = new Vector3();

        private List<string> sLoadedSource = new List<string>();
        private bool sProcessing = false;
        private bool sCompiled = false;

        private Vector3 sParentPos;

        public void ssLOAD_SOURCE(TextAsset src, bool resetToDefault)
        {
            if (string.IsNullOrEmpty(src.text)) return;
            sLoadedSource = new List<string>();

            sCompiled = false;
            foreach (string line in src.text.Split(new[] { System.Environment.NewLine }, System.StringSplitOptions.None))
                sLoadedSource.Add(line);

            if (!sCompiled) ssCOMPILE();

            ssRESET_ELEMENTS_TO_DEFAULT(resetToDefault);
        }

        public void ssPROCESS_ANIMATION()
        {
            if (sLoadedSource == null) return;
            if (sLoadedSource.Count == 0) return;

            if (sProcessing) StopAllCoroutines();
            sProcessing = false;

            if (!sCompiled) ssCOMPILE();

            ssRESET_ELEMENTS_TO_DEFAULT();
            StartCoroutine(animProcessor());
        }

        public void ssRESET_ELEMENTS_TO_DEFAULT(bool leaveAnimMode = false)
        {
            if (leaveAnimMode) StopAllCoroutines();

            foreach (sElementBuffer e in sAnimElementsBuffer)
            {
                if (e.src == null) continue;
                if (e.inAnimOnly) e.src.gameObject.SetActive(!leaveAnimMode);
                e.src.localPosition = e.oPos;
                e.src.localRotation = Quaternion.Euler(e.oRot);
                e.src.localScale = e.oSca;
            }

            if (leaveAnimMode) transform.localPosition = sParentPos;
            else transform.localPosition = sOffset;
        }

        private void ssCOMPILE()
        {
            sAnimElementsBuffer = new List<sElementBuffer>();

            //-----Converting ASCII code to Elements array buffer
            for (int i = 1; i < sLoadedSource.Count; i++)
            {
                if (string.IsNullOrEmpty(sLoadedSource[i])) continue;

                if(i == 1 || i == 2 || i == 3)
                {
                    string[] content = sLoadedSource[i].Split(';');
                    switch(i)
                    {
                        //-------Setting up basic element buffer
                        case 1:
                            foreach (string c in content)
                            {
                                string[] varib = c.Split('=');
                                if (varib.Length != 2) continue;
                                sAnimElementsBuffer.Add(new sElementBuffer() { varName = varib[0].Trim(), varValue = varib[1].Trim() });
                            }
                            break;

                        //-------Setting up essential animation settings
                        case 2:
                            if (content.Length != 3) continue;
                            sLoopAnim = content[0].Trim() == "Loop";
                            float.TryParse(content[1].Trim(), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out sLifeTime);
                            sOffset = ssReturnCommandVector(content[2], out _);
                            break;

                        //-------Getting 'Animation-Only-Related-Objects' list
                        case 3:
                            foreach (string cont in content)
                            {
                                for (int zz = 0; zz < sAnimElementsBuffer.Count; zz++)
                                {
                                    if (sAnimElementsBuffer[zz].varName != cont) continue;

                                    sElementBuffer e = sAnimElementsBuffer[zz];
                                    e.inAnimOnly = true;
                                    sAnimElementsBuffer[zz] = e;
                                    break;
                                }
                            }
                            break;
                    }
                    continue;
                }

                //-----Command buffer
                string[] ccontent = sLoadedSource[i].Split('=');
                if (ccontent.Length != 2) continue;

                string vName = ccontent[0].Trim();
                string vCommand = ccontent[1];

                for (int x = 0; x < sAnimElementsBuffer.Count; x++)
                {
                    if (sAnimElementsBuffer[x].varName != vName) continue;

                    sElementBuffer eB = sAnimElementsBuffer[x];

                    int cType;
                    Vector3 vecRes = ssReturnCommandVector(vCommand, out cType);
                    if (cType == 0) eB.dPos = vecRes;
                    if (cType == 1) eB.dRot = vecRes;
                    if (cType == 2) eB.dSca = vecRes;

                    sAnimElementsBuffer[x] = eB;
                }
            }

            //-----Receiving generic physical objects
            int z = 0;
            while(z < sAnimElementsBuffer.Count)
            {
                sElementBuffer b = sAnimElementsBuffer[z];
                foreach(Transform t in transform.GetComponentsInChildren<Transform>(true))
                {
                    if(t.name == b.varValue)
                    {
                        b.src = t;
                        b.oPos = t.localPosition;
                        b.oRot = t.localRotation.eulerAngles;
                        b.oSca = t.localScale;
                        break;
                    }
                }
                sAnimElementsBuffer[z] = b;
                z++;
            }
            sParentPos = transform.localPosition;

            sCompiled = true;
        }

        private Vector3 ssReturnCommandVector(string entry, out int cType)
        {
            entry = entry.Trim();
            cType = entry.StartsWith("M") ? 0 : entry.StartsWith("R") ? 1 : entry.StartsWith("S") ? 2 : 3;
            entry = cType == 3 ? entry : entry.Remove(0, 1);

            string[] ec = entry.Split(',');
            if (ec.Length != 3) return Vector3.zero;

            float x = ssParseFloat(ec[0].Trim());
            float y = ssParseFloat(ec[1].Trim());
            float z = ssParseFloat(ec[2].Trim());

            return new Vector3(x,y,z);
        }
        private float ssParseFloat(string input)
        {
            float outer;
            float.TryParse(input, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture , out outer);
            return outer;
        }

        private IEnumerator animProcessor()
        {
            sProcessing = true;
            float t = 0.0f;
            float s = sLifeTime;
            while (t < s)
            {
                foreach(sElementBuffer e in sAnimElementsBuffer)
                {
                    if(e.src == null)
                    {
                        Debug.Log("Procedural animator: Object " + e.varName + " is missing oe couldn't be found");
                        continue;
                    }
                    if(e.dPos != Vector3.zero)
                        e.src.Translate(e.dPos * Time.deltaTime, Space.Self);
                    if (e.dRot != Vector3.zero)
                        e.src.Rotate(e.dRot * Time.deltaTime, Space.Self);
                    if (e.dSca != Vector3.zero)
                        e.src.localScale += e.dSca * Time.deltaTime;
                }
                t += Time.deltaTime;
                yield return null;
            }

            if(sLoopAnim)
            {
                ssRESET_ELEMENTS_TO_DEFAULT();
                StartCoroutine(animProcessor());
            }
        }
    }
}