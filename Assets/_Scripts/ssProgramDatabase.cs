﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yahart
{
    public class ssProgramDatabase : MonoBehaviour
    {
        [System.Serializable]
        public class TemporaryClass
        {
            public string sName;
            public GameObject sSource;
            public TextAsset[] sAnimations;
            public List<GameObject> sAngles;
            public string[] sInformation;
            public List<Material> sParts;
        }
        public TemporaryClass[] temporaryClass;

        [Space]

        public Transform tempObjectsRoot;
        private List<GameObject> TempNoze = new List<GameObject>();

        private void Awake()
        {
            TempNoze.Clear();
            for (int i = 0; i < tempObjectsRoot.childCount; i++)
                TempNoze.Add(tempObjectsRoot.GetChild(i).gameObject);

            for (int i = 0; i < TempNoze.Count; i++)
            {
                temporaryClass[i].sSource = TempNoze[i];
                if (TempNoze[i].transform.childCount == 0) continue;

                if (!TempNoze[i].transform.Find("Source")) continue;

                temporaryClass[i].sAngles = new List<GameObject>();
                temporaryClass[i].sParts = new List<Material>();

                Transform roviny = TempNoze[i].transform.Find("Source").Find("Roviny");
                Transform casti = TempNoze[i].transform.Find("Source").Find("Casti");

                if (roviny)
                    foreach (Transform t in roviny.transform)
                    {
                        temporaryClass[i].sAngles.Add(t.gameObject);
                    }

                if (casti)
                    foreach (Transform t in casti.transform)
                    {
                        foreach (Material mat in t.GetComponent<Renderer>().materials)
                            temporaryClass[i].sParts.Add(mat);
                    }
            }

            foreach(TemporaryClass t in temporaryClass)
            {
                if (!t.sSource.GetComponent<ssProceduralAnimator>()) continue;
                if (t.sAnimations.Length == 0) continue;
                t.sSource.GetComponent<ssProceduralAnimator>().ssLOAD_SOURCE(t.sAnimations[0], true);
            }
        }
    }
}