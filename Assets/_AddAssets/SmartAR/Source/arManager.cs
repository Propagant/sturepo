﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SmartAR
{
    [AddComponentMenu("Matej Vanco/Smart AR/AR Manager")]

    /// <summary>
    /// Essential system for SmartAR written by Matej Vanco 2020
    /// Modular AR system editor with internal editor at realtime
    /// Please read the official docs for more info
    /// </summary>
    public class arManager : MonoBehaviour
    {
        //--------------------------------------------------AR Manager General Input & Components
        public ARSession arSessionComponent;
        public bool arUseStreaming = true;
        public arStreaming arStreamingComponent;
        public bool arStreamingLoadOnStart = false;
        public bool arUsePerformanceStats = true;
        public arPerformanceStats arPerformanceStatsComponent;

        //--------------------------------------------------AR Editor Essential Variables
        public bool arEditor_EditableSpecificObjects = true;
        public List<Transform> arEditor_SpecificObjects = new List<Transform>();
        public enum arEditor_FindObjectsBy_ { Name, Tag, NameMacro, Component};
        public arEditor_FindObjectsBy_ arEditor_FindObjectsBy = arEditor_FindObjectsBy_.Component;
        public string arEditor_findObjNameTagMacro;
        public MonoBehaviour arEditor_findObjComponent;
        //AR Editor Values & Abstracts
        public Transform arEditor_SelectedObject;

        public float arEditor_MoveMultiplier = 0.2f;
        public float arEditor_RotationMultiplier = 12.0f;
        public float arEditor_ScaleMultiplier = 0.05f;

        public Vector2 arEditor_MoveMultiLimitation = new Vector2(0.01f, 0.5f);
        public Vector2 arEditor_RotationMultiLimitation = new Vector2(1.0f, 20.0f);
        public Vector2 arEditor_ScaleMultiLimitation = new Vector2(0.01f, 0.1f);

        public bool arEditor_RotationMode = false;
        public bool arEditor_MoveDimensionXY = false;
        public bool arEditor_AlignFromUserView = true;

        public bool arEditor_EnableEditorOnStart = false;

        //--------------------------------------------------AR Editor UI Hierarchy
        [System.Serializable]
        public class arEditorUI_
        {
            [Header("Essential Editor UI")]
            public GameObject uiFullEditorUI;
            public Transform uiSelectionPointer;
            public bool useInformationalTextDebug = true;
            public Text uiInformationalText;
            [Header("Editor Speed Values UI")]
            public Text uiMoveRotMultiply;
            public Text uiScaleSpeed;
            [Header("Internal Editor Info UI")]
            public Text uiEditorInfo;
            public Text uiEditorSelectedObjInfo;
            [Header("Editor Hierarchy UI")]
            public Button uiHierarchy_ButtonPrefab;
            public Transform uiHierarchy_ContentRoot;
        }
        public arEditorUI_ arEditorUI;

        [System.Serializable]
        public class arCalibration_
        {
            public bool CalibrateOnStart = true;
            public bool PlayCameraRenderDistanceEffect = true;
            public float CameraMaxRenderDistanceEffect = 12f;
            public GameObject CalibrationInfoRoot;
            public GameObject CalibrationButton;
            [Space]
            public bool useMaxSteadyTime = true;
            public float maxSteadyTime = 40f;
            public bool useMaxZeroGyroTime = true;
            public float SteadyMinUnbiasedSpeed = 0.05f;
            public float maxZeroGyroTime = 3f;
        }
        public arCalibration_ arCalibration;

        //-------------------------------------------------INTERNAL VARIABLES
        private bool internalCalibrationProcess = false;

        private Vector3 internalPreviousVal;
        private float internal_StoredMoveSpeed;
        private float internal_StoredRotationSpeed;

        private float arGyro_recalibrationTimer;
        private float arGyro_UnbiasedRate;

        private Camera mainCam;

        private void Awake()
        {
            if (Camera.main == null)
                Debug.LogError("AR Manager: There is no main camera!");
            mainCam = Camera.main;
            Input.gyro.enabled = true;

#if UNITY_EDITOR
            if (arCalibration.useMaxSteadyTime)
                arCalibration.maxSteadyTime = Mathf.Infinity;
            if (arCalibration.useMaxZeroGyroTime)
                arCalibration.maxZeroGyroTime = Mathf.Infinity;
#endif
            //----Getting editable objects in scene
            arEditor_RefreshHierarchy();

            arEditorUI.uiFullEditorUI.SetActive(arEditor_EnableEditorOnStart);
            if (arUsePerformanceStats)
                arPerformanceStatsComponent.enabled = arEditorUI.uiFullEditorUI.activeInHierarchy;
            else if (arPerformanceStatsComponent)
                arPerformanceStatsComponent.enabled = false;

            if (arUseStreaming && arStreamingComponent)
            {
                arStreamingComponent.MainArManager = this;
                if (arStreamingLoadOnStart)
                    arStreamingComponent.arLoad();
            }

            internal_StoredRotationSpeed = arEditor_RotationMultiplier;
            internal_StoredMoveSpeed = arEditor_MoveMultiplier;

            //---Setting up calibration [recommended to be enabled!]
            if (!arCalibration.CalibrateOnStart)
            {
                if (arCalibration.PlayCameraRenderDistanceEffect)
                {
                    mainCam.farClipPlane = 0.2f;
                    StartCoroutine(arInternal_CamRenderEffect());
                }
                return;
            }
            if (arCalibration.PlayCameraRenderDistanceEffect)
                mainCam.farClipPlane = 0.2f;
            internalCalibrationProcess = true;
            arCalibration.CalibrationInfoRoot.gameObject.SetActive(true);
            arCalibration.CalibrationButton.SetActive(true);
        }

        /// <summary>
        /// Play additional camera render effect
        /// </summary>
        private IEnumerator arInternal_CamRenderEffect()
        {
            float t = 0.0f;
            while (t < 8.0f)
            {
                mainCam.farClipPlane = Mathf.Lerp(0.2f, arCalibration.CameraMaxRenderDistanceEffect, t / 8.0f);
                t += Time.deltaTime;
                yield return null;
            }
        }


        //AR System Essentials

        /// <summary>
        /// Reset AR Core [recalibrate]
        /// </summary>
        public void ar_ResetARCore()
        {
            internalCalibrationProcess = false;
            arCalibration.CalibrationButton.SetActive(false);
            arCalibration.CalibrationInfoRoot.SetActive(false);

            arSessionComponent.Reset();

            arEditorInternal_DebugInfo("Calibrated");

            if (arCalibration.PlayCameraRenderDistanceEffect)
                StartCoroutine(arInternal_CamRenderEffect());
        }

        /// <summary>
        /// Process to gyroscopic calibration [decalibrated]
        /// </summary>
        public void ar_ProcessToGyroCalibration()
        {
            internalCalibrationProcess = true;
            arGyro_recalibrationTimer = 0f;

            arCalibration.CalibrationButton.SetActive(true);
            arCalibration.CalibrationInfoRoot.SetActive(true);

            this.StopAllCoroutines();

            if(arCalibration.PlayCameraRenderDistanceEffect)
                mainCam.farClipPlane = 0.2f;
        }

        //AR Editor System

        #region AR_EditorSystem

        /// <summary>
        /// Editor - essential object selection
        /// </summary>
        public void arEditor_SelectObject(GameObject obj)
        {
            arEditorUI.uiSelectionPointer.gameObject.SetActive(true);
            arEditor_SelectedObject = obj.transform;
        }
        /// <summary>
        /// Editor - essential object deselection
        /// </summary>
        public void arEditor_DeselectObject()
        {
            if (arEditor_SelectedObject)
                arEditorInternal_DebugInfo("Object deselected");
            arEditor_SelectedObject = null;
            arEditorUI.uiSelectionPointer.gameObject.SetActive(false);
        }

        /// <summary>
        /// Editor - turn off/on
        /// </summary>
        public void arEditor_OnOff(bool onoff)
        {
            arEditorUI.uiFullEditorUI.SetActive(onoff);
            if (arUsePerformanceStats)
                arPerformanceStatsComponent.enabled = arEditorUI.uiFullEditorUI.activeInHierarchy;
            else if (arPerformanceStatsComponent)
                arPerformanceStatsComponent.enabled = false;
        }

        /// <summary>
        /// Editor - change move/rotation multiply value
        /// </summary>
        public void arEditor_ChangeMoveRotMulti(Slider s)
        {
            if (arEditor_RotationMode)
            {
                arEditor_RotationMultiplier = s.value;
                internal_StoredRotationSpeed = arEditor_RotationMultiplier;
                s.minValue = arEditor_RotationMultiLimitation.x;
                s.maxValue = arEditor_RotationMultiLimitation.y;
                arEditorUI.uiMoveRotMultiply.text = "Rotation Multiplier " + arEditor_RotationMultiplier.ToString("0.00");
            }
            else
            {
                arEditor_MoveMultiplier = s.value;
                internal_StoredMoveSpeed = arEditor_MoveMultiplier;
                s.minValue = arEditor_MoveMultiLimitation.x;
                s.maxValue = arEditor_MoveMultiLimitation.y;
                arEditorUI.uiMoveRotMultiply.text = "Movement Multiplier " + arEditor_MoveMultiplier.ToString("0.00");
            }
        }
        /// <summary>
        /// Editor - change scale multiply value
        /// </summary>
        public void arEditor_ChangeScaleMulti(Slider s)
        {
            arEditor_ScaleMultiplier = s.value;
            s.minValue = arEditor_ScaleMultiLimitation.x;
            s.maxValue = arEditor_ScaleMultiLimitation.y;
            arEditorUI.uiScaleSpeed.text = "Scale " + arEditor_ScaleMultiplier.ToString("0.00");
        }

        /// <summary>
        /// Editor - change object size [true - add, false - subtr]
        /// </summary>
        public void arEditor_ChangeObjectSize(bool add)
        {
            if (!arEditor_SelectedObject)
            {
                arEditorInternal_DebugInfo("There is no object selected");
                return;
            }
            float v = (add) ? arEditor_ScaleMultiplier : -arEditor_ScaleMultiplier;
            Vector3 objs = arEditor_SelectedObject.transform.localScale;
            objs.x += v;
            objs.y += v;
            objs.z += v;
            arEditor_SelectedObject.transform.localScale = objs;
        }

        /// <summary>
        /// Editor - change editor XY dimension
        /// </summary>
        public void arEditor_ChangeXYDimension(bool XY_)
        {
            arEditor_MoveDimensionXY = XY_;
            arEditorInternal_DebugInfo("XY Dimension: " + XY_.ToString());
        }
        /// <summary>
        /// Editor - change to/from rotation mode
        /// </summary>
        public void arEditor_ChangeToRotationMode(bool rotMode)
        {
            arEditor_RotationMode = rotMode;

            if (arEditor_RotationMode)
            {
                arEditor_MoveMultiplier = internal_StoredRotationSpeed;
                arEditorUI.uiMoveRotMultiply.text = "Rotation Multiplier " + arEditor_MoveMultiplier.ToString("0.00");
            }
            else
            {
                arEditor_MoveMultiplier = internal_StoredMoveSpeed;
                arEditorUI.uiMoveRotMultiply.text = "Movement Multiplier " + arEditor_MoveMultiplier.ToString("0.00");
            }
            arEditorInternal_DebugInfo("Rotation Mode: " + arEditor_RotationMode.ToString());
        }
        /// <summary>
        /// Editor - change global align
        /// </summary>
        public void arEditor_ChangeAlign(bool align)
        {
            arEditor_AlignFromUserView = align;
            arEditorInternal_DebugInfo("Align From User: " + arEditor_AlignFromUserView.ToString());
        }
        /// <summary>
        /// Editor - change global align [switch version]
        /// </summary>
        public void arEditor_ChangeAlignSwitch()
        {
            arEditor_AlignFromUserView = !arEditor_AlignFromUserView;
            arEditorInternal_DebugInfo("Align From User: " + arEditor_AlignFromUserView.ToString());
        }

        /// <summary>
        /// Editor - reset object transform [tType = 0-location,1-rotation,2-scale]
        /// </summary>
        public void arEditor_ResetTrans(int tType)
        {
            if (!arEditor_SelectedObject)
            {
                arEditorInternal_DebugInfo("Nothing to reset");
                return;
            }
            if (tType == 0)
                arEditor_SelectedObject.transform.localPosition = Vector3.zero;
            else if (tType == 1)
                arEditor_SelectedObject.transform.localRotation = Quaternion.identity;
            else
                arEditor_SelectedObject.transform.localScale = Vector3.one;
        }

        /// <summary>
        /// Editor - refresh current hierarchy content
        /// </summary>
        public void arEditor_RefreshHierarchy()
        {
            if(arEditorUI.uiHierarchy_ContentRoot.childCount != 0)
            {
                for (int i = arEditorUI.uiHierarchy_ContentRoot.childCount - 1; i >= 0; i--)
                    Destroy(arEditorUI.uiHierarchy_ContentRoot.GetChild(i).gameObject);
            }

            if (arEditor_EditableSpecificObjects)
            {
                foreach (Transform gm in arEditor_SpecificObjects)
                {
                    Button newBut = Instantiate(arEditorUI.uiHierarchy_ButtonPrefab, arEditorUI.uiHierarchy_ContentRoot);
                    newBut.gameObject.SetActive(true);
                    newBut.onClick.AddListener(delegate { arEditor_SelectObject(gm.gameObject); });
                    newBut.GetComponentInChildren<Text>().text = gm.name;
                }
            }
            else
            {
                arEditor_SpecificObjects.Clear();
                switch (arEditor_FindObjectsBy)
                {
                    case arEditor_FindObjectsBy_.Name:
                        foreach (Transform gm in GameObject.FindObjectsOfType(typeof(Transform)))
                        {
                            if (gm.name != arEditor_findObjNameTagMacro)
                                continue;
                            Button newBut = Instantiate(arEditorUI.uiHierarchy_ButtonPrefab, arEditorUI.uiHierarchy_ContentRoot);
                            newBut.gameObject.SetActive(true);
                            newBut.onClick.AddListener(delegate { arEditor_SelectObject(gm.gameObject); });
                            newBut.GetComponentInChildren<Text>().text = gm.name;
                            arEditor_SpecificObjects.Add(gm);
                        }
                        break;
                    case arEditor_FindObjectsBy_.Tag:
                        foreach (Transform gm in GameObject.FindObjectsOfType(typeof(Transform)))
                        {
                            if (gm.tag != arEditor_findObjNameTagMacro)
                                continue;
                            Button newBut = Instantiate(arEditorUI.uiHierarchy_ButtonPrefab, arEditorUI.uiHierarchy_ContentRoot);
                            newBut.gameObject.SetActive(true);
                            newBut.onClick.AddListener(delegate { arEditor_SelectObject(gm.gameObject); });
                            newBut.GetComponentInChildren<Text>().text = gm.name;
                            arEditor_SpecificObjects.Add(gm);
                        }
                        break;
                    case arEditor_FindObjectsBy_.NameMacro:
                        foreach (Transform gm in GameObject.FindObjectsOfType(typeof(Transform)))
                        {
                            if (!gm.name.Contains(arEditor_findObjNameTagMacro))
                                continue;
                            Button newBut = Instantiate(arEditorUI.uiHierarchy_ButtonPrefab, arEditorUI.uiHierarchy_ContentRoot);
                            newBut.gameObject.SetActive(true);
                            newBut.onClick.AddListener(delegate { arEditor_SelectObject(gm.gameObject); });
                            newBut.GetComponentInChildren<Text>().text = gm.name;
                            arEditor_SpecificObjects.Add(gm);
                        }
                        break;
                    case arEditor_FindObjectsBy_.Component:
                        foreach (Transform gm in GameObject.FindObjectsOfType(typeof(Transform)))
                        {
                            if (!gm.GetComponent(arEditor_findObjComponent.name))
                                continue;
                            Button newBut = Instantiate(arEditorUI.uiHierarchy_ButtonPrefab, arEditorUI.uiHierarchy_ContentRoot);
                            newBut.gameObject.SetActive(true);
                            newBut.onClick.AddListener(delegate { arEditor_SelectObject(gm.gameObject); });
                            newBut.GetComponentInChildren<Text>().text = gm.name;
                            arEditor_SpecificObjects.Add(gm);
                        }
                        break;
                }
            }
        }

        public void arEditorInternal_DebugInfo(string text)
        {
            if (!arEditorUI.useInformationalTextDebug)
                return;
            if (arEditorUI.uiInformationalText == null)
                return;
            arEditorUI.uiInformationalText.gameObject.SetActive(false);
            arEditorUI.uiInformationalText.gameObject.SetActive(true);
            arEditorUI.uiInformationalText.text = text;
            arEditorUI.uiInformationalText.GetComponent<Animation>().Play();
        }

        #endregion

        /// <summary>
        /// AR Editor - selection logic
        /// </summary>
        private void arEditorEssential_ProcessSelectorLogic()
        {
            if (!arEditor_SelectedObject)
                return;
            Vector3 p = mainCam.WorldToScreenPoint(arEditor_SelectedObject.position);
            p.z = 0f;
            Vector3 screenPoint = mainCam.WorldToViewportPoint(arEditor_SelectedObject.position);
            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
            arEditorUI.uiSelectionPointer.GetComponent<Image>().enabled = onScreen;
            arEditorUI.uiSelectionPointer.position = p;
        }

        /// <summary>
        /// AR Editor - rotation logic
        /// </summary>
        private void arEditorEssential_ProcessRotation()
        {
            if (Input.touchCount != 1)
                return;

            Touch touch = Input.GetTouch(0);
            Vector3 tPos = touch.deltaPosition;

            if (touch.phase == TouchPhase.Began)
                internalPreviousVal = tPos;

            Vector3 tcRot = (tPos - internalPreviousVal);
            Vector3 fRot = (arEditor_AlignFromUserView) ? arEditor_SelectedObject.transform.InverseTransformDirection(mainCam.transform.TransformDirection(new Vector3(tcRot.y, -tcRot.x, 0))) : new Vector3(0, -tcRot.x, 0);
            if (arEditor_AlignFromUserView)
                arEditor_SelectedObject.Rotate(fRot * arEditor_RotationMultiplier * Time.deltaTime);
            else
                arEditor_SelectedObject.localEulerAngles += fRot * arEditor_RotationMultiplier * Time.deltaTime;
        }

        /// <summary>
        /// AR Editor - location logic
        /// </summary>
        private void arEditorEssential_ProcessRelocation()
        {
            if (Input.touchCount != 1)
                return;

            Touch touch = Input.GetTouch(0);
            Vector3 tPos = touch.deltaPosition;

            if (touch.phase == TouchPhase.Began)
                internalPreviousVal = tPos;

            Vector3 tcPos = (tPos - internalPreviousVal);
            Vector3 fPos = (arEditor_MoveDimensionXY) ? new Vector3(tcPos.x, tcPos.y, 0) : new Vector3(tcPos.x, 0, tcPos.y);
            if (arEditor_AlignFromUserView) fPos = mainCam.transform.TransformDirection(fPos);
            arEditor_SelectedObject.position += fPos * arEditor_MoveMultiplier * Time.deltaTime;
        }

        private void Update()
        {
            arInternal_CalibrationChecker();

            if (!arEditorUI.uiFullEditorUI.activeInHierarchy)
                return;

            arEditorEssential_ProcessSelectorLogic();

            arEditorUI.uiEditorInfo.text = "Gyroscope: " + Input.gyro.attitude.x.ToString("0.0") + "X " + Input.gyro.attitude.y.ToString("0.0") + "Y\n" +
                "Bias: " + arGyro_UnbiasedRate.ToString("0.00");
            if (arCalibration.useMaxSteadyTime || arCalibration.useMaxZeroGyroTime)
            {
                arEditorUI.uiEditorInfo.text += "\n\nCondition timer: " + arGyro_recalibrationTimer.ToString("0.0");
                if(arCalibration.useMaxSteadyTime)
                    arEditorUI.uiEditorInfo.text += "\nMax Steady Time " + arCalibration.maxSteadyTime.ToString("0.0");
                if (arCalibration.useMaxZeroGyroTime)
                    arEditorUI.uiEditorInfo.text += "\nMax ZeroGyro Time " + arCalibration.maxZeroGyroTime.ToString("0.0");
            }

            if (arEditor_SelectedObject)
            {
                arEditorUI.uiEditorSelectedObjInfo.text = "<b>"+arEditor_SelectedObject.name +"</b>"+
                    "\nPosition: " + arEditor_SelectedObject.transform.position.ToString() +
                    "\nRotation: " + arEditor_SelectedObject.transform.eulerAngles.ToString() +
                    "\nScale: " + arEditor_SelectedObject.transform.localScale.ToString();

                if (Input.touchCount == 0)
                    return;
                Touch touch = Input.touches[0];
                if (UnityEngine.EventSystems.EventSystem.current != null && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    return;

                if (!arEditor_RotationMode)
                    arEditorEssential_ProcessRelocation();
                else
                    arEditorEssential_ProcessRotation();
            }
            else if (!string.IsNullOrEmpty(arEditorUI.uiEditorSelectedObjInfo.text))
                arEditorUI.uiEditorSelectedObjInfo.text = string.Empty;
        }

        //Calibration Checker

        /// <summary>
        /// Check calibration conditions ['when to calibrate']
        /// </summary>
        private void arInternal_CalibrationChecker()
        {
            //----Calibration Checker
            if (!internalCalibrationProcess)
            {
                bool gyroA = ((arCalibration.useMaxZeroGyroTime) ? (Mathf.Abs(Input.gyro.attitude.x) < 0.1f && Mathf.Abs(Input.gyro.attitude.y) < 0.1f) : false);
                bool gyroB = ((arCalibration.useMaxSteadyTime) ? arGyro_UnbiasedRate <= arCalibration.SteadyMinUnbiasedSpeed : false);
                if (gyroA || gyroB)
                {
                    arGyro_recalibrationTimer += Time.deltaTime;
                    float max = arCalibration.maxSteadyTime;
                    if (gyroA)
                        max = arCalibration.maxZeroGyroTime;
                    else if (gyroB)
                        max = arCalibration.maxSteadyTime;
                    if (arGyro_recalibrationTimer >= max)
                        ar_ProcessToGyroCalibration();
                }
                else if (arGyro_recalibrationTimer != 0) arGyro_recalibrationTimer = 0;
            }
            arGyro_UnbiasedRate = Mathf.Lerp(arGyro_UnbiasedRate, Input.gyro.rotationRateUnbiased.magnitude, 0.521f * Time.deltaTime); //---0.521 can be changed, depending on how fast will be the value changing
        }       
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(arManager))]
    public class arManagerEditor : Editor
    {
        private arManager targetm;

        private void OnEnable()
        {
            targetm = (arManager)target;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Space(10f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arSessionComponent", "Session Component");
            DrawProperty("arUseStreaming", "Use Streaming", "Use save/load feature");
            if (targetm.arUseStreaming)
            {
                DrawProperty("arStreamingComponent", "Streaming Component");
                DrawProperty("arStreamingLoadOnStart", "Load On Start","Load data from streaming after start if possible");
            }
            DrawProperty("arUsePerformanceStats", "Use Performance Stats");
            if (targetm.arUsePerformanceStats)
                DrawProperty("arPerformanceStatsComponent", "Performance Stats Component");
            GUILayout.EndVertical();

            GUILayout.Space(3f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arEditor_EditableSpecificObjects", "Edit Specific Objects","Choose specific objects or get object by specific macro");
            if (targetm.arEditor_EditableSpecificObjects)
            {
                GUILayout.BeginVertical("Box");
                DrawProperty("arEditor_SpecificObjects", "Specific Game Objects","",true);
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.BeginVertical("Box");
                DrawProperty("arEditor_FindObjectsBy", "Find Objects By", "Select an option that will get your objects");
                switch(targetm.arEditor_FindObjectsBy)
                {
                    case arManager.arEditor_FindObjectsBy_.Name:
                        DrawProperty("arEditor_findObjNameTagMacro", "Find Objects by Name");
                        break;
                    case arManager.arEditor_FindObjectsBy_.NameMacro:
                        DrawProperty("arEditor_findObjNameTagMacro", "Find Objects by Name Macro","If objects name contain this character");
                        break;
                    case arManager.arEditor_FindObjectsBy_.Tag:
                        DrawProperty("arEditor_findObjNameTagMacro", "Find Objects by Tag");
                        break;
                    case arManager.arEditor_FindObjectsBy_.Component:
                        DrawProperty("arEditor_findObjComponent", "Find Objects by THIS Component");
                        break;
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();

            GUILayout.Space(3f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arEditor_MoveMultiplier", "Editor Move Multiplier", "Selected object moving speed proceed by finger");
            DrawProperty("arEditor_RotationMultiplier", "Editor Rotation Multiplier", "Selected object rotation speed proceed by finger");
            DrawProperty("arEditor_ScaleMultiplier", "Editor Scaling Multiplier", "Selected object scaling speed proceed by finger");
            GUILayout.Space(2f);
            DrawProperty("arEditor_MoveMultiLimitation", "Move Multi Limitation", "Min & Max movement multiplication");
            DrawProperty("arEditor_RotationMultiLimitation", "Rot Multi Limitation", "Min & Max rotation multiplication");
            DrawProperty("arEditor_ScaleMultiLimitation", "Scale Multi Limitation", "Min & Max scale multiplication");

            DrawProperty("arEditor_RotationMode", "Rotation Mode", "If enabled, the rotation mode will be enabled");
            GUILayout.EndVertical();

            GUILayout.Space(3f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arEditor_MoveDimensionXY", "Move Dimenstion XY","If enabled, the movement direction will be set to X and Y axis");
            DrawProperty("arEditor_AlignFromUserView", "Align From User View","Align transform-changing function from users perspective");
            DrawProperty("arEditor_EnableEditorOnStart", "Enable Editor On Start","Show editor on startup");
            GUILayout.EndVertical();

            GUILayout.Space(10f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arEditorUI", "Editor UI","",true);
            GUILayout.EndVertical();

            GUILayout.Space(10f);

            GUILayout.BeginVertical("Box");
            DrawProperty("arCalibration", "Calibration Settings","",true);
            GUILayout.EndVertical();

            GUILayout.Space(5f);
        }

        private void DrawProperty(string s, string Text, string ToolTip = "", bool includeChilds = false)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(s), new GUIContent(Text, ToolTip), includeChilds, null);
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}