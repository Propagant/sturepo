﻿using UnityEngine;
using System.Collections;

namespace Yahart
{
    [AddComponentMenu("Matej Vanco/Smart AR/AR Object Observer")]

    /// <summary>
    /// Additional object observer/ viewer system written by Matej Vanco 2020
    /// Drag object and rotate by moving with finger on screen
    /// </summary>
    public class arAdd_ObjectObserver : MonoBehaviour
    {
        private Camera mainCam;
        private Vector3 prevtPos;

        public ssMainMenu mainMenu;
        [Space]
        public float observ_RotSpeed = 4f;
        public float observ_ZoomSpeed = 1.5f;
        public float observ_OffsetSpeed = 0.05f;
        public float observ_ResizeSpeed = 0.05f;
        [Space]
        public float observ_SmoothLoc = 0.24f;
        [Space]
        public Vector3 observ_Offset;
        public float observ_OriginDepth = 1.8f;
        public float observ_Borders = 0.4f;
        [Space]
        public float observ_LastZoom = 70;

        public bool observ_FeatureProcess = true;

        private float observ_CurrentDistance;
        private float observ_LastDistance;

        public float observ_CurrentZoom;
        public float observ_CurrentSize;

        [HideInInspector] public Quaternion startRot;
        [HideInInspector] public Vector3 startOff;

        public void Awake()
        {
            if (Camera.main == null) Debug.LogError("AR Observer: There is no main camera!");
            mainCam = Camera.main;
            observ_CurrentZoom = 70;
            observ_CurrentSize = 0.4f;
            startRot = transform.localRotation;
            startOff = observ_Offset;
        }

        private void Start()
        {
            ProcessFeature(0);
        }

        private void LateUpdate()
        {
            observ_LastDistance = observ_CurrentDistance;
        }

        private Touch t0, t1;

        private IEnumerator featureProcess()
        {
            observ_FeatureProcess = false;
            yield return new WaitForSeconds(0.1f);
            observ_FeatureProcess = true;
        }

        private void Update()
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, observ_SmoothLoc);

            if(!mainMenu.sInAR) mainCam.fieldOfView = Mathf.Lerp(mainCam.fieldOfView, observ_CurrentZoom, 4.0f * Time.deltaTime);
            else transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * observ_CurrentSize, 4.0f * Time.deltaTime);

            int touchCount = Input.touchCount;
            if (touchCount == 0 || !observ_FeatureProcess) return;

            if (UnityEngine.EventSystems.EventSystem.current != null && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                return;

            switch (touchCount)
            {
                case 1:
                    t0 = Input.GetTouch(0);
                    if (t0.phase == TouchPhase.Ended)
                        StartCoroutine(featureProcess());
                    ProcessFeature(0);
                    break;
                case 2:
                    t0 = Input.GetTouch(0);
                    t1 = Input.GetTouch(1);
                    if (t0.phase == TouchPhase.Ended)
                        StartCoroutine(featureProcess());
                    ProcessFeature(1);
                    break;
                case 3:
                    t0 = Input.GetTouch(0);
                    if (t0.phase == TouchPhase.Ended)
                        StartCoroutine(featureProcess());
                    ProcessFeature(2);
                    break;

                default: return;
            }
        }

        [HideInInspector] public float xOf, yOf;

        private void ProcessFeature(int featureIndex)
        {
            switch(featureIndex)
            {
                case 0:
                    Vector3 tPos = t0.deltaPosition;

                    if (t0.phase == TouchPhase.Began)
                        prevtPos = tPos;

                    Vector3 tcRot = (tPos - prevtPos);
                    xOf += -tcRot.y * observ_RotSpeed * Time.deltaTime;
                    yOf += tcRot.x * observ_RotSpeed * Time.deltaTime;
                    Quaternion dir = Quaternion.Euler(xOf, yOf, 0);
                    Vector3 add = new Vector3(0, 0, -1.8f);
                    mainCam.transform.position = dir * add + transform.position + observ_Offset;
                    mainCam.transform.rotation = dir;
                    break;

                case 1:
                    
                    if(t0.phase != TouchPhase.Moved || t1.phase != TouchPhase.Moved)
                        return;

                    Vector2 touch0, touch1;
                    touch0 = t0.position;
                    touch1 = t1.position;
                    observ_CurrentDistance = Vector2.Distance(touch0, touch1) / 10;
                    observ_LastZoom = observ_CurrentDistance;

                    if (mainMenu.sInAR)
                    {
                        observ_CurrentSize += (observ_LastDistance > observ_CurrentDistance ? -observ_LastZoom : observ_LastZoom) * observ_ResizeSpeed * Time.deltaTime;
                        observ_CurrentSize = Mathf.Clamp(observ_CurrentSize, 0.1f, 0.5f);
                        return;
                    }

                    observ_CurrentZoom += (observ_LastDistance > observ_CurrentDistance ? observ_LastZoom : -observ_LastZoom) * observ_ZoomSpeed * Time.deltaTime;
                    observ_CurrentZoom = Mathf.Clamp(observ_CurrentZoom, 40, 75);
                    break;

                case 2: return;
                    Vector3 tMove = observ_Offset;

                    if (mainMenu.sInAR)
                    {
                        tMove += new Vector3(t0.deltaPosition.x, 0, t0.deltaPosition.y) * observ_OffsetSpeed * Time.deltaTime;
                        observ_Offset = tMove;
                        return;
                    }

                    tMove += new Vector3(t0.deltaPosition.x, t0.deltaPosition.y, 0) * (observ_CurrentZoom * 0.01f) * observ_OffsetSpeed * Time.deltaTime;
                    tMove.z = 0;
                    tMove.x = Mathf.Clamp(tMove.x, -observ_Borders, observ_Borders);
                    tMove.y = Mathf.Clamp(tMove.y, -observ_Borders, observ_Borders);
                    observ_Offset = tMove;
                    break;
            }
        }

        public void ResetToOriginalTransform()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one * 0.4f;
            observ_LastZoom = 70;
            observ_CurrentZoom = 70;
            transform.Rotate(45, -90, 45);
            observ_Offset = Vector3.zero;
            observ_Offset.z = observ_OriginDepth;
        }

        public void ResetToARTransform()
        {

        }
    }
}