﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace SmartAR
{
    [AddComponentMenu("Matej Vanco/Smart AR/AR Streaming")]
    [RequireComponent(typeof(arManager))]
    /// <summary>
    /// Specific save-load system for AR App written by Matej Vanco
    /// Streaming transform property only!
    /// </summary>
    public class arStreaming : MonoBehaviour
    {
        [HideInInspector] public string MainGeneratedPath;
        [HideInInspector] public arManager MainArManager;

        public bool UsePersistentDataPath = true;
        [Space]
        [Tooltip("Set main root directory name")] public string RootFolderName = "Data";
        [Tooltip("Set main extension name without dot!")] public string MainExtension = "txt";
        [Tooltip("Set main file name without dot!")] public string DefaultFileName = "AREditorData";
        [System.Serializable]
        public class additionalMessages
        {
            public string Message_Saved = "AR Editor Saved";
            public string Message_Loaded = "AR Editor Loaded";
            public string Message_NothingToLoad = "Nothing To Load";
            public string Message_Removed = "AR Editor Data Removed";
            public string Message_Interrupted = "AR Editor Data Interrupted";
        }
        [Space]
        [Tooltip("Customize internal messages in specific cases...")]public additionalMessages AdditionalMessages;
        [HideInInspector] public Transform[] ObjectsToStream;

        private void checkData()
        {
            string rootPath = (UsePersistentDataPath) ? Application.persistentDataPath : Application.dataPath;

            RootFolderName = RootFolderName.Replace(".", "").Replace("/","");
            MainExtension = MainExtension.Replace(".", "").Replace("/", "");
            DefaultFileName = DefaultFileName.Replace(".", "").Replace("/", "");

            rootPath += "/" + RootFolderName;
            try
            {
                if (!Directory.Exists(rootPath))
                    Directory.CreateDirectory(rootPath);
            }
            catch(IOException e)
            {
                EDebug(e.Message);
                return;
            }
            rootPath += "/" + DefaultFileName + "." + MainExtension;
            try
            {
                if (!File.Exists(rootPath))
                    File.Create(rootPath).Dispose();
            }
            catch (IOException e)
            {
                EDebug(e.Message);
                return;
            }

            MainGeneratedPath = rootPath;
        }
        private void EDebug(string s)
        {
            Debug.LogError("Streaming error: "+s);
        }

        /// <summary>
        /// Streaming system save specific objects
        /// </summary>
        public void arSave()
        {
            checkData();

            ObjectsToStream = MainArManager.arEditor_SpecificObjects.ToArray();

            try
            {
                FileStream fs = new FileStream(MainGeneratedPath, FileMode.Open);
                StreamWriter wr = new StreamWriter(fs);
                wr.WriteLine(System.DateTime.Now.ToLongDateString());

                for (int i = 0; i < ObjectsToStream.Length; i++)
                {
                    Transform gm = ObjectsToStream[i];

                    Vector3 gP = gm.localPosition;
                    Quaternion gR = gm.localRotation;
                    Vector3 gS = gm.localScale;

                    wr.WriteLine($"{gP.x.ToString().Replace(",", ".")},{gP.y.ToString().Replace(",", ".")},{gP.z.ToString().Replace(",", ".")}/" +
                        $"{gR.x.ToString().Replace(",", ".")},{gR.y.ToString().Replace(",", ".")},{gR.z.ToString().Replace(",", ".")},{gR.w.ToString().Replace(",", ".")}/" +
                        $"{gS.x.ToString().Replace(",", ".")},{gS.y.ToString().Replace(",", ".")},{gS.z.ToString().Replace(",", ".")}");
                }

                wr.Dispose();
                fs.Dispose();
            }
            catch(IOException e)
            {
                EDebug("INTERNAL [WHILE SAVING] ERROR: " + e.Message);
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Interrupted + "\n" +e.Message);
                return;
            }
            MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Saved);
        }

        /// <summary>
        /// Streaming system clear saved data
        /// </summary>
        public void arClearAllData()
        {
            try
            {
                File.WriteAllText(MainGeneratedPath, "");
            }
            catch (IOException e)
            {
                EDebug("INTERNAL [WHILE REMOVING DATA] ERROR: " + e.Message);
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Interrupted + "\n" +e.Message);
                return;
            }
            MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Removed);
        }

        /// <summary>
        /// Streaming system load specific objects
        /// </summary>
        public void arLoad()
        {
            checkData();

            ObjectsToStream = MainArManager.arEditor_SpecificObjects.ToArray();

            if (!File.Exists(MainGeneratedPath))
            {
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_NothingToLoad);
                return;
            }
            if (File.ReadAllText(MainGeneratedPath).Length == 0)
            {
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_NothingToLoad);
                return;
            }
            if (File.ReadAllLines(MainGeneratedPath).Length < 2)
            {
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_NothingToLoad);
                return;
            }

            try
            {
                string[] allLines = File.ReadAllLines(MainGeneratedPath);
                for (int i = 1; i < allLines.Length; i++)
                {
                    string fLine = allLines[i];
                    string[] parts = fLine.Split('/');
                    if (parts.Length != 3)
                    {
                        Debug.Log("Object skipped " + i);
                        continue;
                    }

                    string[] pos = parts[0].Split(',');
                    string[] rot = parts[1].Split(',');
                    string[] sca = parts[2].Split(',');

                    Vector3 ppos = new Vector3(returnF(pos[0]), returnF(pos[1]), returnF(pos[2]));
                    Quaternion rrot = new Quaternion(returnF(rot[0]), returnF(rot[1]), returnF(rot[2]), returnF(rot[3]));
                    Vector3 ssca = new Vector3(returnF(sca[0]), returnF(sca[1]), returnF(sca[2]));

                    Transform g = ObjectsToStream[i - 1];
                    g.localPosition = ppos;
                    g.localRotation = rrot;
                    g.localScale = ssca;
                }
            }
            catch (IOException e)
            {
                EDebug("INTERNAL [WHILE LOADING] ERROR: " + e.Message);
                MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Interrupted + "\n" + e.Message);
                return;
            }
            MainArManager.arEditorInternal_DebugInfo(AdditionalMessages.Message_Loaded);
        }

        private float returnF(string inp)
        {
            float ff;
            float.TryParse(inp, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out ff);
            return ff;
        }
    }
}